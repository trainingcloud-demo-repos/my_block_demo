<?php

namespace Drupal\my_block_demo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A block that displays the current user's name.
 *
 * @Block(
 *   id = "user_info_block",
 *   admin_label = @Translation("User info block"),
 * )
 */
class UserInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $current_user = \Drupal::currentUser();

    $build['user_info'] = [
      '#markup' => $this->t('You are logged in as @name', [
        '@name' => $current_user->getDisplayName(),
      ]),
    ];

    $build['#cache']['contexts'][] = 'user';
    $build['#cache']['tags'][] = 'user:' . $current_user->id();

    return $build;
  }

}
