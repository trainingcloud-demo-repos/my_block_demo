<?php

namespace Drupal\my_block_demo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A block that displays the current date.
 *
 * @Block(
 *   id = "date_block",
 *   admin_label = @Translation("Date block"),
 * )
 */
class DateBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $request_time = \Drupal::time()->getCurrentTime();

    $date = $this->t('Hi, today is @weekday of week @weeknum of @year.', [
      '@weekday' => date('l', $request_time),
      '@weeknum' => date('W', $request_time),
      '@year' => date('Y', $request_time),
    ]);

    $time = $this->t('The time is @time.', [
      '@time' => date('H:i:s', $request_time),
    ]);

    $build['date'] = [
      '#markup' => $date . ' ' . $time,
    ];

    // Setting the max-age to 0 here means this block won't be cached.
    $build['#cache'] = ['max-age' => 0];

    return $build;
  }

}
